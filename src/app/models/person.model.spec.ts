import { Person } from './person.model';

fdescribe('Test para PersonModel', ()=>{//Describe se puede hacer set o conjuntos de pruebas
    describe('Test para el método person.getFullName', () => {//subconjuto

        it('Return un string con name + lastname', () => {
          // ARRANGE
          const person = new Person('David', 'Sandoval', 15);
          // ACT
          const rta = person.getFullName();
          // ASSERT
          expect(rta).toEqual('David-Sandoval');
        });
    });

    describe('Test para el método person.getAgeInYears', () => {

        xit('Return 35 años', () => {//es un escenario que el codigo cumpla, q el code resuelva. describe q debe hacer, contenido de cada test.
          // Arrange -Preparar
          const person = new Person('David', 'Sandoval', 25);
          // Act -Actuar
          const age = person.getAgeInYears(10);
          // Assert- Afirmar
          expect(age).toEqual(35);//valido que el escenario se resuelva o haga match con lo q se va a resolver
        });
        it('Return 40 años', () => {
            // Arrange
            const person = new Person('David', 'Sandoval', 25);
            // Act
            const age = person.getAgeInYears(15);
            // Assert
            expect(age).toEqual(40);
        });
        it('Return 25 años cuando la suma es numeros negativos', () => {
            // Arrange
            const person = new Person('David', 'Sandoval', 25);
            // Act
            const age = person.getAgeInYears(-10);
            // Assert
            expect(age).toEqual(25);
          });
    });
});
